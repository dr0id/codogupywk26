import sys

import pygame
from pygame.locals import *
pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((350, 350))
screen_rect = screen.get_rect()

pygame.display.set_caption('Mouse: pick color | Space: Save | Escape: Abort')

# water blue = 0, 134, 139
bg_color = 0, 134, 139

TO_COLOR = 255, 255, 255, 0

data = (
    # (fm_color, to_color, src_file, dest_file),
    (TO_COLOR, 'branch1.bmp', 'branch1.png'),
    (TO_COLOR, 'branch2.bmp', 'branch2.png'),
)

for to_color, fm_file, to_file in data:
    print('CONVERTING...')
    print('From: {}'.format(fm_file))
    print('To  : {}'.format(to_file))
    print('')
    if fm_file == to_file:
        raise Exception('error: loaded and saved images cannot be same file')

    # load image
    fm_img = pygame.image.load(fm_file)
    fm_rect = fm_img.get_rect()

    # scale image
    scale_me = False
    if scale_me:
        # scale = float(128) / img.get_width()
        scale = 0.5
        fm_img = pygame.transform.rotozoom(fm_img, 0.0, scale)

    # make dest surface
    w, h = fm_img.get_size()
    to_img = pygame.Surface((w, h + 1), SRCALPHA)
    to_img.fill((0, 0, 0, 0), None, BLEND_MULT)
    px1 = Rect(0, 0, 1, 1)

    done = False
    while not done:
        screen.fill(bg_color)
        screen.blit(fm_img, (0, 0))
        pygame.display.flip()

        fm_color = None
        while fm_color is None:
            clock.tick(10)
            for e in pygame.event.get():
                if e.type == MOUSEMOTION:
                    if fm_rect.collidepoint(e.pos):
                        c = fm_img.get_at(e.pos)
                        pygame.display.set_caption('Pixel color: {}'.format(c))
                elif e.type == MOUSEBUTTONDOWN:
                    fm_color = fm_img.get_at(e.pos)
                    # copy pixels w/ alpha conversion
                    for x in range(fm_img.get_width()):
                        for y in range(fm_img.get_height()):
                            c = fm_img.get_at((x, y))
                            if c == fm_color:
                                c.a = 0
                                c.r = c.g = c.b = 255
                            px1.topleft = x, y
                            to_img.fill((0, 0, 0, 0), px1, BLEND_RGBA_MULT)
                            to_img.fill(c, px1, BLEND_RGBA_ADD)
                    print('Replaced {}'.format(fm_color))
                    fm_img = to_img
                    fm_rect = fm_img.get_rect()
                elif e.type == KEYDOWN:
                    if e.key == K_ESCAPE:
                        quit()
                    elif e.key == K_SPACE:
                        # pygame.image.save(to_img, to_file)
                        done = True
                elif e.type == QUIT:
                    quit()
