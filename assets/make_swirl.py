# -*- coding: utf-8 -*-
"""swirl.py
"""
import sys
from math import cos, sin, pi

import pygame
from pygame.locals import *


def make_swirl(width, height, color):
    img = pygame.Surface((width, height), SRCALPHA)
    img.fill((0, 0, 0, 0), None, BLEND_MULT)

    xoffset = w // 2
    yoffset = h // 2

    def translate(p):
        return int(round(p[0] + xoffset)), int(round(p[1] + yoffset))

    last_point = (0, 0)
    delta_theta = 0.01   # Radians, Make smaller for a finer resolution
    num_turns = 4
    constant_a = 0
    constant_b = 1

    points = [translate(last_point)]
    theta = 0.0
    while theta < num_turns * 2 * pi:
        this_r = constant_a + constant_b * theta
        this_point = this_r * cos(theta), this_r * sin(theta)
        print(translate(last_point), translate(this_point))
        # pygame.draw.aaline(img, color, translate(last_point), translate(this_point), 1)
        points.append(translate(this_point))
        last_point = this_point
        theta += delta_theta
    p0 = points[0]
    for p1 in points[1:]:
        pygame.draw.line(img, color, p0, p1, 2)
        p0 = p1
    return img


pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((400, 400))
screen_rect = screen.get_rect()

w, h = 50, 50
img = make_swirl(w, h, Color('white'))
pygame.image.save(img, 'swirl.png')
bg_color = 0, 0, 0

while 1:
    clock.tick(30)
    screen.fill(bg_color)
    for e in pygame.event.get():
        if e.type == KEYDOWN:
            quit()
    screen.blit(img, (0, 0))
    pygame.display.flip()
