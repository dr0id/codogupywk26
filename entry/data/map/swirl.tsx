<?xml version="1.0" encoding="UTF-8"?>
<tileset name="swirl" tilewidth="50" tileheight="50" tilecount="1" columns="1">
 <properties>
  <property name="kind" value="swirl"/>
 </properties>
 <image source="../graphics/swirl.png" width="50" height="50"/>
 <tile id="0">
  <properties>
   <property name="kind" value="swirl"/>
  </properties>
 </tile>
</tileset>
