<?xml version="1.0" encoding="UTF-8"?>
<tileset name="gator" tilewidth="64" tileheight="32" tilecount="4" columns="1">
 <image source="../graphics/gator4-64x128.png" width="64" height="129"/>
 <tile id="0">
  <properties>
   <property name="kind" value="gator"/>
  </properties>
  <animation>
   <frame tileid="2" duration="200"/>
   <frame tileid="1" duration="200"/>
   <frame tileid="0" duration="200"/>
   <frame tileid="3" duration="200"/>
  </animation>
 </tile>
 <tile id="1">
  <properties>
   <property name="kind" value="gator"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="kind" value="gator"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="kind" value="gator"/>
  </properties>
 </tile>
</tileset>
