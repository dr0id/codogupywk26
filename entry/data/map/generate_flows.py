# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'generate_flows.py' is part of codogupywk26
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

import pygame

from pyknic.mathematics import Point3 as Point

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2018"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")


def main():
    pygame.init()
    screen = pygame.display.set_mode((1024, 756))
    # screen.fill((255, 255, 255))
    # colors = list(pygame.color.THECOLORS.values())
    colors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080', '#ffffff', '#000000']
    colors = [pygame.color.Color(c) for c in colors]
    font = pygame.font.Font(None, 15)

    tile_size = 32
    angle_step = 15
    speeds = [0.1, 0.25, 0.5, 0.75, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0]
    assert len(speeds) <= len(colors), "not enough colors!"

    bodies = []
    tile_id = 0
    kind = "velocity"
    angles = range(0, 360, angle_step)
    columns = len(angles)
    rows = len(speeds)
    tile_count = columns * rows
    surf = pygame.Surface((columns * tile_size, rows * tile_size), pygame.SRCALPHA)
    surf.fill((0, 0, 0, 0))

    for idx, speed in enumerate(speeds):
        for idx2, angle in enumerate(angles):

            v = Point(1.0, 0)
            # v.length = speed / max(speeds) * tile_size / 2.0
            v.length = tile_size / 2.0
            v.rotate(angle)

            f = 50 / max(speeds)  # f * ms = 15
            w = int(speed / max(speeds) * f)
            if w < 1:
                w = 1

            color = colors[idx]

            tile_surf = pygame.Surface((tile_size, tile_size), pygame.SRCALPHA)
            tile_rect = tile_surf.get_rect()
            tile_surf.fill((0, 0, 0, 0))

            pygame.draw.line(tile_surf, color, tile_rect.center,
                             (int(tile_rect.centerx + v.x), int(tile_rect.centery + v.y)), w)

            txt_surf = font.render(str(speed), 1, (255, 255, 255))
            txt_rect = txt_surf.get_rect()
            txt_angle = font.render(str(angle), 1, (255, 255, 255))
            txt_rect_angle = txt_angle.get_rect(midtop=txt_rect.midbottom)

            r = txt_rect.union(txt_rect_angle)
            r.center = tile_rect.center

            txt_rect.midtop = r.midtop
            tile_surf.blit(txt_surf, txt_rect)

            txt_rect_angle.midbottom = r.midbottom
            tile_surf.blit(txt_angle, txt_rect_angle)

            surf.blit(tile_surf, (idx2 * tile_size, idx * tile_size))


            body_template = """
 <tile id="{0}">
  <properties>
   <property name="kind" value="{1}"/>
   <property name="speed" value="{2}"/>
   <property name="angle" value="{3}"/>
  </properties>
 </tile>""".format(tile_id, kind, speed, angle)
            bodies.append(body_template)
            tile_id += 1



    name = "velocities"
    png_name = "%s.png" % name
    pygame.image.save(surf, png_name)

    header = """<?xml version="1.0" encoding="UTF-8"?>
<tileset name="{0}" tilewidth="{2}" tileheight="{2}" tilecount="{3}" columns="{4}">
 <image source="{1}" width="{5}" height="{6}"/>""".format(name, png_name, tile_size, tile_count, columns, columns * tile_size,
                                                          rows * tile_size)
    footer = "</tileset>"



    xml_content = header + "".join(bodies) + footer
    with open(name+".tsx", "w") as xml:
        xml.write(xml_content)

    screen.blit(surf, (0, 0))
    pygame.display.flip()
    pygame.event.clear()
    pygame.event.wait()


if __name__ == "__main__":
    main()

logger.debug("imported")
