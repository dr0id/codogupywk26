<?xml version="1.0" encoding="UTF-8"?>
<tileset name="branch1" tilewidth="48" tileheight="63" tilecount="1" columns="1">
 <properties>
  <property name="kind" value="branch"/>
 </properties>
 <image source="../graphics/branch1.png" width="48" height="63"/>
 <tile id="0" type="branch">
  <properties>
   <property name="kind" value="branch"/>
  </properties>
 </tile>
</tileset>
