﻿Deep Breath - Pyweek 26, October 2018
=====================================

CONTACT: IRC DR0ID_, Gummbum @ freenode.net #pyweek #pygame

Homepage: https://pyweek.org/e/CoDoGuPywk26/
Name: Deep Breath
Team: CoDoGuPywk26
Members: DR0ID, gummbum


DEPENDENCIES:

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/

Tested with Python 2.7 and 3.6.


DESCRIPTION:
============

You're a guy or girl on a Mission. There's only a River and all its Perils in
between you and the Security of the Realm. What are you waiting for, Whelp? :)


RUNNING THE GAME:
=================

On Windows or Mac OS X, locate the "run.pyw" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run.py


HOW TO PLAY THE GAME:
=====================

"A" and "D" turn the boat one step at a time.

"S" flips the boat down, and "S" flips it back. The boat also auto-uprights if
the captain is underwater too long.

"W" jumps to catch a second of air. This is essential for not tumbling
wrecklessly over waterfalls. It's better to go over the edge with Purpose. :)

Quit the game at any time by clicking the window's Close (X) button.


TROUBLE:
========

If the game crashes and you wish to assist us with a bug report, you can enable
debug level logging by editing gamelib/settings.py, change:

  # change this setting to DEBUG
  log_level = [logging.WARNING, logging.DEBUG][1]

Rerun the program until the error occurs, then contact DR0ID_ or Gummbum to
provide the run_debug.py.log file.


LICENSE:
========

This game has same license as pyknic. Some of the content is reused under fair
use principle and is subject to its own restrictions.


CHEATING AND TWEAKING:
======================

1. Master volume (settings.py).

  master_volume adjusts the level of all sound (0.0 to 1.0).

  If you want to tweak music or sfx volume individually, manually hack
  gamelib/resource_sound.py. It should be pretty easy to figure out.

2. Window size (settings.py).

  screen_width and screen_height adjust the window size. Custom sizes have not
  been play-tested, but they appear to work. You may prefer 800x600, 640x480,
  or a square shape for a performance gain.

3. Startup level (settings.py).

  startup_level dictates which map to start with. If you're having trouble with
  a particular map, or need to interrupt the game, this setting is for you.

4. Damage values (settings.py).

  There are DAMAGE_* settings at the end of the file.
