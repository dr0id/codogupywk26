# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'canoe.py' is part of codogupywk26
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The canoe entity module.


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging
import random

from gamelib import settings
from gamelib import resource_sound
from gamelib.eventing import event_dispatcher
from gamelib.settings import KIND_CANOE, EVENT_COL_C_WALL, EVENT_COL_C_WALL_ROT, EVENT_COL_C_WATERFALL, \
    EVENT_COL_C_BRIDGE, \
    DAMAGE_CANOE_WALL, DAMAGE_CANOE_WALL_ROTATE, DAMAGE_CANOE_WATERFALL_CRASH, DAMAGE_CANOE_LOG, EVENT_COL_C_SWIRL, \
    EVENT_GATOR_BITE, DAMAGE_GATOR_BITE, canoe_jump_height
from pyknic.ai.statemachines import StateDrivenAgentBase, BaseState
from pyknic.mathematics import Point3 as Point, Vec3 as Vec, sign

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2018"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Canoe", "Watered", "Airborne", "Flipped", "WaterfallFalling"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class CanoeBaseState(BaseState):
    @staticmethod
    def handle_event(owner, event, *args):
        if EVENT_COL_C_WALL == event:
            canoe, kinds, wall = args
            owner.inflict_damage(DAMAGE_CANOE_WALL, canoe, kinds, wall)
            settings.sfx_handler.handle_message(random.choice(resource_sound.SFX_HITS), 'mutex')
        elif EVENT_COL_C_WALL_ROT == event:
            canoe, kinds, wall = args
            owner.inflict_damage(DAMAGE_CANOE_WALL_ROTATE, canoe, kinds, str(wall) + " rotation")
            settings.sfx_handler.handle_message(random.choice(resource_sound.SFX_HITS), 'mutex')
        elif EVENT_COL_C_WATERFALL == event:
            canoe, kinds, waterfall = args
            if owner.waterfall != waterfall:
                owner.waterfall = waterfall
                owner.water_level = owner.position.y + waterfall.free_fall / settings.PPU
                owner.state_machine.switch_state(WaterfallFalling)
        elif event == EVENT_COL_C_BRIDGE:
            canoe, kinds, bridge = args
            owner.state_machine.switch_state(Flipped)
            owner.inflict_damage(DAMAGE_CANOE_LOG, canoe, kinds, bridge)
            settings.sfx_handler.handle_message(random.choice(resource_sound.SFX_HITS), 'mutex')
        elif event == EVENT_COL_C_SWIRL:
            canoe, swirl = args
            owner.tumble_duration = swirl.duration
            owner.state_machine.switch_state(Tumbling)
        elif event == EVENT_GATOR_BITE:
            canoe, kinds, gator = args
            owner.inflict_damage(DAMAGE_GATOR_BITE, canoe, kinds, gator)
            settings.sfx_handler.handle_message(random.choice(resource_sound.SFX_GATOR_GROWLS), 'mutex')
        else:
            logger.warning("canoe unhandled event received: {0} {1}", event, args)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        v = owner.v
        # f = owner.v.normalized.dot(owner.water_v.normalized)
        # v = owner.v * (1.2 - f)
        owner.old_position.copy_values(owner.position)
        owner.position += (v + owner.water_v) * dt
        settings.sfx_handler.handle_message(random.choice(resource_sound.SFX_PADDLES), 'queue')

    @staticmethod
    def on_timer(owner, *args):
        pass


class Tumbling(CanoeBaseState):

    @staticmethod
    def enter(owner):
        owner.timer.start(abs(settings.tumble_time_span), True)
        owner.set_current_animation(owner.animations[0], False)

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def handle_event(owner, event, *args):
        if event == settings.ACTION_MOVE_LEFT:
            owner.v = owner.v.rotated(-settings.rotation_angle, Vec(0, 0, 1))
            return
        elif event == settings.ACTION_MOVE_RIGHT:
            owner.v = owner.v.rotated(settings.rotation_angle, Vec(0, 0, 1))
            return
        elif event == settings.ACTION_JUMP:
            # owner.state_machine.switch_state(Airborne)
            return
        elif event == settings.ACTION_FLIP:
            owner.state_machine.switch_state(Flipped)
            return
        elif event == EVENT_GATOR_BITE:
            canoe, kinds, gator = args
            owner.inflict_damage(DAMAGE_GATOR_BITE, canoe, kinds, gator)

    @staticmethod
    def on_timer(owner, *args):
        owner.state_machine.switch_state(Watered)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        if random.random() < 1.0 / abs(owner.tumble_duration):
            rot_dir = -sign(owner.tumble_duration)
            owner.v.rotate(settings.rotation_angle * rot_dir)
        CanoeBaseState.update(owner, dt, sim_t, *args, **kwargs)


class Airborne(CanoeBaseState):
    @staticmethod
    def handle_event(owner, event, *args):
        if event == settings.ACTION_MOVE_LEFT:
            return
        elif event == settings.ACTION_MOVE_RIGHT:
            return
        elif event == settings.ACTION_JUMP:
            return
        elif event == settings.ACTION_FLIP:
            return
        elif event == EVENT_COL_C_WATERFALL:
            canoe, kinds, waterfall = args
            if owner.waterfall != waterfall:
                owner.waterfall = waterfall
                owner.water_level = owner.position.y + waterfall.free_fall / settings.PPU
            return
        elif event == EVENT_GATOR_BITE:
            return  # do not bite in air?
        CanoeBaseState.handle_event(owner, event, *args)

    @staticmethod
    def enter(owner):
        owner.position.z = -canoe_jump_height
        owner.water_level = owner.position.y
        owner.set_current_animation(owner.animations[1], True)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        g = Vec(0, 0, settings.gravitation)
        v = owner.v
        # f = owner.v.normalized.dot(owner.water_v.normalized)
        # v = owner.v * (1.2 - f)
        v += g * dt
        owner.position += (v + owner.water_v) * dt
        if owner.position.y + owner.position.z >= owner.water_level:
            owner.position.y = owner.water_level
            owner.position.z = 0
            owner.v.z = 0
            owner.state_machine.switch_state(Watered)


class Watered(CanoeBaseState):
    @staticmethod
    def enter(owner):
        owner.set_current_animation(owner.animations[0], False)
        settings.sfx_handler.handle_message(random.choice(resource_sound.SFX_GATOR_SPLASHES), 'mutex')

    @staticmethod
    def handle_event(owner, event, *args):
        if event == settings.ACTION_MOVE_LEFT:
            owner.v = owner.v.rotated(-settings.rotation_angle, Vec(0, 0, 1))
            return
        elif event == settings.ACTION_MOVE_RIGHT:
            owner.v = owner.v.rotated(settings.rotation_angle, Vec(0, 0, 1))
            return
        elif event == settings.ACTION_JUMP:
            owner.state_machine.switch_state(Airborne)
            settings.sfx_handler.handle_message(resource_sound.SFX_PLAYER_JUMP)
            return
        elif event == settings.ACTION_FLIP:
            owner.state_machine.switch_state(Flipped)
            settings.sfx_handler.handle_message(resource_sound.SFX_PLAYER_SUBMERGE)
            return
        elif event == EVENT_COL_C_WATERFALL:
            canoe, kinds, waterfall = args
            if id(owner.waterfall) == id(waterfall):
                if owner not in waterfall.done:
                    waterfall.done.add(owner)  # inflict only once
                    # owner.inflict_damage(DAMAGE_CANOE_WATERFALL_CRASH, canoe, kinds, waterfall)

        CanoeBaseState.handle_event(owner, event, *args)


class Flipped(CanoeBaseState):
    @staticmethod
    def enter(owner):
        # start timer
        owner.timer.start(settings.under_water_update_interval, True)
        owner.left_time_span_under_water = settings.max_under_water_time_span_in_seconds
        owner.set_current_animation(owner.animations[2], True)
        owner.set_current_animation(owner.animations[3])

    @staticmethod
    def exit(owner):
        # stop timer
        owner.timer.stop()
        owner.left_time_span_under_water = settings.max_under_water_time_span_in_seconds
        owner.set_current_animation(owner.animations[4], True)

    @staticmethod
    def handle_event(owner, event, *args):
        if event == settings.ACTION_MOVE_LEFT:
            return
        elif event == settings.ACTION_MOVE_RIGHT:
            return
        elif event == settings.ACTION_JUMP:
            return
        elif event == settings.ACTION_FLIP:
            owner.state_machine.switch_state(Watered)
            return
        elif event == EVENT_COL_C_BRIDGE:
            return
        CanoeBaseState.handle_event(owner, event, *args)

    @staticmethod
    def on_timer(owner, *args):
        owner.left_time_span_under_water -= settings.under_water_update_interval
        if owner.left_time_span_under_water <= 0:
            owner.left_time_span_under_water = 0
            owner.state_machine.switch_state(Watered)


class Sunken(CanoeBaseState):

    @staticmethod
    def enter(owner):
        owner.set_current_animation(owner.animations[-2], True)
        owner.set_current_animation(owner.animations[-1], False)

    @staticmethod
    def handle_event(owner, event, *args):
        return


class WaterfallFalling(CanoeBaseState):

    @staticmethod
    def enter(owner):
        owner.set_current_animation(owner.animations[0], False)
        settings.sfx_handler.handle_message(resource_sound.SFX_WATERFALL_CRASH)

    @staticmethod
    def exit(owner):
        # inflict waterfall damage here and only here!
        owner.inflict_damage(DAMAGE_CANOE_WATERFALL_CRASH, owner, (owner.kind, owner.waterfall.kind), owner.waterfall)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        g = Vec(0, 0, settings.gravitation)
        v = owner.v
        # f = owner.v.normalized.dot(owner.water_v.normalized)
        # v = owner.v * (1.2 - f)
        v += g * dt
        owner.position += (v + owner.water_v) * dt
        if owner.position.y + owner.position.z >= owner.water_level:
            owner.position.y = owner.water_level
            owner.position.z = 0
            owner.v.z = 0
            owner.state_machine.switch_state(Tumbling)

    @staticmethod
    def handle_event(owner, event, *args):
        return


class Canoe(StateDrivenAgentBase):
    kind = KIND_CANOE

    def __init__(self, timer):
        """
        Canoe constructor.
        """
        StateDrivenAgentBase.__init__(self, Watered, self)
        # eventing.event_dispatcher.add_listener(settings.EVENT_TEST, self.handle_event)
        self.position = Point(0, 0, 0)
        self.old_position = Point(0, 0, 0)
        self.water_v = Vec(0.0, 0.0, 0.0)  # m/s
        self.v = Vec(0.0, 0.0, 0.0)  # m/s
        self.timer = timer
        self.timer.event_elapsed += self._on_timer
        self.left_time_span_under_water = settings.max_under_water_time_span_in_seconds
        self._health = 100
        self.water_level = 0
        self.old_water_level = 0
        self.waterfall = None
        self.tumble_duration = 1.0
        event_dispatcher.add_listener(EVENT_COL_C_WALL, self.handle_event)
        event_dispatcher.add_listener(EVENT_COL_C_WALL_ROT, self.handle_event)
        event_dispatcher.add_listener(EVENT_COL_C_WATERFALL, self.handle_event)
        event_dispatcher.add_listener(EVENT_COL_C_BRIDGE, self.handle_event)
        event_dispatcher.add_listener(EVENT_COL_C_SWIRL, self.handle_event)
        event_dispatcher.add_listener(EVENT_GATOR_BITE, self.handle_event)

        self.state_machine.event_switched_state += self._on_state_switched

        self.animations = []  # normal, jump, flip, flipped, flip up, sink, sunken
        self._anim_queue = []
        self.damage_factor = 1.0

    def append_animation(self, anim):
        anim.canoe = self
        self.animations.append(anim)
        anim.position = self.position
        anim.stop()
        anim.visible = False

    def set_current_animation(self, anim, immediate=False, start=False):
        if self._anim_queue and anim == self._anim_queue[0]:
            return

        if immediate:
            for to_remove in self._anim_queue[1:]:
                to_remove.stop()
                to_remove.reset()
                to_remove.visible = False
                to_remove.event_animation_end -= self._on_anim_ended

            self._anim_queue[1:] = []

        self._anim_queue.append(anim)
        if start:
            anim.start()
            anim.visible = True
            anim.event_animation_end += self._on_anim_ended

        if immediate:
            self._on_anim_ended()

        # TODO: SFX code goes here?

    def _on_anim_ended(self, *args):
        if len(self._anim_queue) > 1:
            to_remove = self._anim_queue.pop(0)
            to_remove.stop()
            to_remove.reset()
            to_remove.visible = False
            to_remove.event_animation_end -= self._on_anim_ended
            current = self._anim_queue[0]
            current.reset()
            current.start()
            current.visible = True
            current.event_animation_end += self._on_anim_ended

    def _on_state_switched(self, *args):
        owner, prev_state, sm = args
        logger.info("Canoe state change: {0} -> {1}", prev_state, self.state_machine.current_state)

    @property
    def health(self):
        return self._health

    def inflict_damage(self, damage, canoe, kinds, other):
        if canoe == self:
            self._health -= damage * self.damage_factor
            logger.info("canoe {0}: new health {1} due to {2}{3}", id(self), self._health, kinds, other)
            if self._health <= 0:
                self._health = 0
                self.state_machine.switch_state(Sunken)

    def _on_timer(self, *args):
        self.state_machine.current_state.on_timer(self, *args)

    def handle_event(self, event, *args):
        canoe = args[0]
        if canoe == self:
            StateDrivenAgentBase.handle_event(self, event, *args)


logger.debug("imported")
