# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'sfx.py' is part of CoDoGuPywk24
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Module for handling sfx and music.

Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

from pyknic.entity import TypedEntity
from gamelib import settings

import pygame

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class SoundData(object):
    def __init__(self, filename, volume, reserved_channel_id=None):
        self.filename = '{}/{}'.format(settings.sfx_path, filename)
        self.volume = volume
        self.reserved_channel_id = reserved_channel_id
        self.channel = None
        self.sound = None


class MusicData(object):
    def __init__(self, filename, volume):
        self.filename = '{}/{}'.format(settings.music_path, filename)
        self.volume = volume


class Sound(TypedEntity):
    def __init__(self):
        TypedEntity.__init__(self, settings.KIND_SFX)
        self.sound_data = {}
        self.queued_music = None
        self.music_carousel = []
        self.music_paused = False

    def fadeout(self, sfx_id, ms):
        sound_data = self.sound_data.get(sfx_id, None)
        if sound_data is not None:
            if sound_data.channel is not None:
                sound_data.channel.fadeout(ms)
            else:
                sound_data.sound.fadeout(ms)

    def load(self, data_map):
        if pygame.mixer.get_init() is None:
            return
        for ident, data in data_map.items():
            if isinstance(data, SoundData):
                data.sound = pygame.mixer.Sound(data.filename)
                data.sound.set_volume(data.volume)
                if data.reserved_channel_id is not None:
                    data.channel = pygame.mixer.Channel(data.reserved_channel_id)
            else:
                logger.error('not sound data')
            self.sound_data[ident] = data

    # def handle_message(self, sender, receiver_id, msg_type, extra):
    def handle_message(self, msg_type, extra=None):
        """play a sound or song

        If msg_type is MusicData then the song is queued (note that the queueing is done in Python; unlike pygame, the
        queued song will start playing if the current song is faded out).

        If msg_type is SoundData, the object (at instantiation) determines whether the sound plays on a reserved
        channel. If the extra argument is 'queue', then the sound is queued (this is ideal for looping).

        # :param sender: None; not used
        # :param receiver_id: None; not used
        :param msg_type: MusicData or SoundData
        :param extra: None, 'mutex', or 'queue' ('queue' is ignored unless SoundData uses a reserved channel)
        :return:
        """
        if pygame.mixer.get_init() is None:
            return
        data = self.sound_data.get(msg_type)
        if isinstance(data, SoundData):
            if data.channel is not None:
                if extra == 'queue':
                    data.sound.set_volume(data.volume * settings.master_volume)
                    data.channel.queue(data.sound)
                elif extra == 'mutex':
                    if not data.channel.get_busy():
                        data.sound.set_volume(data.volume * settings.master_volume)
                        data.channel.play(data.sound)
                else:
                    data.sound.set_volume(data.volume * settings.master_volume)
                    data.channel.play(data.sound)
            elif extra == 'queue':
                channel = pygame.mixer.find_channel()  # <<< use True to preempt if all channels are busy
                if channel:
                    data.sound.set_volume(data.volume * settings.master_volume)
                    channel.play(data.sound)
            else:
                data.sound.set_volume(data.volume * settings.master_volume)
                data.sound.play()
        elif isinstance(data, MusicData):
            pygame.mixer.music.fadeout(1000)
            self.queued_music = data
        else:
            logger.debug('sfx msg_type not found: {}', msg_type)

    def fill_music_carousel(self, song_ids):
        for ident in song_ids:
            self.music_carousel.append(self.sound_data[ident])

    def clear_music_carousel(self):
        del self.music_carousel[:]

    def start_music_carousel(self):
        if not self.music_carousel:
            return
        pygame.mixer.music.fadeout(1000)
        s = self.music_carousel.pop(0)
        self.queued_music = s
        self.music_carousel.append(s)

    def pause_music(self):
        pygame.mixer.music.pause()
        self.music_paused = True

    def unpause_music(self):
        pygame.mixer.music.unpause()
        self.music_paused = False

    def update(self, dt, sim_t):
        if not self.music_paused and self.queued_music is not None and not pygame.mixer.music.get_busy():
            data = self.queued_music
            pygame.mixer.music.load(data.filename)
            pygame.mixer.music.set_volume(data.volume * settings.master_volume)
            pygame.mixer.music.play()
            self.queued_music = None
            if self.music_carousel:
                s = self.music_carousel.pop(0)
                self.queued_music = s
                self.music_carousel.append(s)

        # This is called from the scheduler
        return settings.sfx_step

settings.sfx_handler = Sound()

logger.debug("imported")
