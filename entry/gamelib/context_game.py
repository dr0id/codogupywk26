# -*- coding: utf-8 -*-
import logging

import pygame

from gamelib import settings, collision
from gamelib.animation import SpriteAnimation, SpecialAnimation
from gamelib.collision import Collider
from gamelib.entities import walledge
from gamelib.entities.canoe import Canoe, Watered, Flipped, Airborne, WaterfallFalling, Sunken
from gamelib.level import Level
from gamelib.settings import gameplay_event_map, ACTION_TOGGLE_DEBUG_RENDER, PPU, canoe_collision_box_size, KIND_CANOE, \
    KIND_GATOR, PIXEL_PPU
from gamelib.skel import hudlight
from gamelib.skel import spritefx
from gamelib.skel.tweening import ease_in_cubic, ease_out_cubic
from gamelib.timestep_context import TimeSteppedContext
from pyknic import timing
from pyknic.mathematics import Vec3, Point3 as Point
from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera
from pyknic.timing import Timer, Scheduler

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Game(TimeSteppedContext):

    def __init__(self, level_num, damage_factor):
        TimeSteppedContext.__init__(self)
        self.level_num = level_num

        self.renderer = DefaultRenderer()
        self.cam = None  # see enter

        self.map_short_name = settings.map_names[level_num]
        self.map_full_name = '/'.join((settings.map_dir, self.map_short_name))
        self.level = Level(self.renderer)

        self._debug_render = False
        self.space = None  # see enter
        self.tweener = None  # see enter

        self.collider = Collider()

        self.scheduler = Scheduler()

        # first player
        # a s d w
        canoe = Canoe(timing.Timer(scheduler=self.scheduler))
        canoe.position.y += 100 / PPU
        self.canoes = [canoe]

        # # add other players
        # # arrows
        # canoe = Canoe(timing.Timer(scheduler=self.scheduler))
        # canoe.position.y += 150 / PPU
        # self.canoes.append(canoe)  # uncomment for second player

        # # no steering
        # canoe = Canoe(timing.Timer(scheduler=self.scheduler))
        # canoe.position.y += 150 / PPU
        # self.canoes.append(canoe)  # uncomment for second player

        self.system_message_info = dict(
            text=None,
            countdown=0.0,
            duration=1.0,
            sprite=None,
            state=0,        # 0: None, 1: enter, 2: wait, 3: exit
        )

        self.hud = hudlight.HUD(**settings.font_themes['gamehud'])
        self.hud.y = 25
        self.hud.add('level', 'Level {}'.format(self.level_num))
        self.hud.add('title', 'Title {}'.format(settings.map_difficulty[self.level_num]))

        self.result = None
        self.damage_factor = damage_factor

    def enter(self):
        TimeSteppedContext.enter(self)

        self.cam = Camera(self._screen.get_rect())
        self.cam.ppu = PIXEL_PPU
        self.cam.rect = self._screen.get_rect()
        # self.cam.ppu = 1.0/PPU
        self.level.load_map(self.map_full_name, self.scheduler)
        self.space = self.level.space
        self.tweener = self.level.tweener
        spritefx.clear()
        del settings.system_messages_queue[:]
        del settings.system_messages_used[:]

        # initialization of canoe velocities
        for idx, c in enumerate(self.canoes):
            c.v = settings.canoe_velocity.clone()
            c.water_v = settings.water_velocity.clone()
            c.position.x = self.level.start_area.centerx / PPU
            c.position.y = self.level.start_area.centery / PPU
            c.water_level = c.position.y
            c.append_animation(self.level.hero_anim_normal[idx])
            c.append_animation(self.level.hero_anim_jump[idx])
            c.append_animation(self.level.hero_anim_flip[idx])
            c.append_animation(self.level.hero_anim_flipped[idx])
            c.append_animation(self.level.hero_anim_flip_up[idx])
            c.append_animation(self.level.hero_anim_sink[idx])
            c.append_animation(self.level.hero_anim_sunken[idx])

            self.level.hero_anim_normal[idx].start()
            c.set_current_animation(self.level.hero_anim_normal[idx], False, True)
            c.damage_factor = self.damage_factor

        self.collider.register((KIND_CANOE, settings.KIND_WALL_EDGE), collision.collide_canoe_vs_wall)
        self.collider.register((KIND_CANOE, settings.KIND_ROCK_EDGE), collision.collide_canoe_vs_rock)
        self.collider.register((KIND_CANOE, settings.KIND_TREE_BRANCH_EDGE), collision.collide_canoe_vs_branch)
        self.collider.register((KIND_CANOE, settings.KIND_FINISH), self._collide_finish)
        self.collider.register((KIND_CANOE, settings.KIND_LOG_EDGE), collision.collide_canoe_bridge)
        self.collider.register((KIND_CANOE, settings.KIND_WATERFALL), collision.collide_canoe_vs_waterfall)
        self.collider.register((KIND_CANOE, settings.KIND_SWIRL), collision.collide_canoe_vs_swirl)
        self.collider.register((KIND_CANOE, settings.KIND_VELOCITY), collision.collide_canoe_vs_velocity)

        self.collider.register((KIND_CANOE, settings.KIND_MESSAGE), collision.collide_canoe_vs_messaging)
        self.collider.register((KIND_CANOE, settings.KIND_START_EDGE), collision.collide_canoe_vs_messaging)
        self.collider.register((KIND_CANOE, settings.KIND_CHECKPOINT_EDGE), collision.collide_canoe_vs_messaging)
        self.collider.register((KIND_CANOE, settings.KIND_GATOR), collision.collide_canoe_vs_gator)

        self.update_cam_position()
        self.update_cam_position()

    def _collide_finish(self, canoe, finishes, kinds):
        finish = list(finishes)[0]
        canoe_pos = canoe.position * PPU
        if pygame.Rect(finish.aabb).collidepoint(*canoe_pos.as_xy_tuple(int)) and canoe.health > 0:
            logger.info(">>>>>>>>>>>>>>> WIN!! <<<<<<<<<<<<<<<<")
            text = getattr(finish, 'collision_text', None)
            sysq = settings.system_messages_queue
            sysu = settings.system_messages_used
            # do once...
            if self.result is None:
                self.result = True
                if text and text not in sysq and text not in sysu:
                    sysq.append(text)

                def _cb_end(*args):
                    self.pop(1)
                    n = self.level_num + 1
                    if n < len(settings.map_names):
                        logger.info('>>>>>>>>>>>>>>> Advance To Level {} <<<<<<<<<<<<<<<<', n)
                        self.push(Game(n, self.damage_factor))
                    else:
                        # TODO: push credits
                        logger.info('>>>>>>>>>>>>>>> GAME WON!! <<<<<<<<<<<<<<<<')
                obj = pygame.sprite.Sprite()
                self.tweener.create_tween_by_end(obj, 'win_countdown', 0.0, 3.0, 4.5, cb_end=_cb_end)

    def update_step(self, delta_time, sim_time, *args):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                self.pop()
        actions, unmapped = gameplay_event_map.get_actions(events)
        for action, extra in actions:
            logger.debug('action {0}, {1}', action, extra)
            if isinstance(action, tuple):
                idx, a = action
                if idx < len(self.canoes):
                    self.canoes[idx].handle_event(a, self.canoes[idx], extra)
            elif action == ACTION_TOGGLE_DEBUG_RENDER:
                self._debug_render = not self._debug_render

        for canoe in self.canoes:
            canoe.update(delta_time, sim_time)

            x = canoe.position.x * PPU - canoe_collision_box_size // 2
            y = canoe.position.y * PPU - canoe_collision_box_size // 2
            others = self.space.get_in_rect(x, y, canoe_collision_box_size, canoe_collision_box_size)
            self.collider.collide_one2many(canoe, others)

        self._update_system_message(delta_time)

        self.update_cam_position()

        self.scheduler.update(delta_time, sim_time)
        self.tweener.update(delta_time)

        settings.sfx_handler.update(delta_time, sim_time)

        self._check_loose()

    def update_cam_position(self):
        # update cam position
        alive_canoes = [c.position for c in self.canoes if c.state_machine.current_state != Sunken]
        if alive_canoes:
            sum_pos = sum(alive_canoes, Vec3(0, 0, 0))
            sum_pos /= len(alive_canoes)
            self.cam.position = sum_pos * PPU

    def _update_system_message(self, delta_time):
        # self.system_message_info = dict(
        #     text=None,
        #     sprite=None,
        #     state=0,        # 0: None, 1: enter, 2: wait, 3: exit
        # )
        def start_message(text):
            spr = spritefx.TextSprite(text, (0, settings.system_message_y), 1.0, 'game', anchor='topright')
            text_half_w = spr.image.get_width() // 2
            text_middle_x = settings.screen_width // 2 + text_half_w
            spr.driftx_effect(0.0, text_middle_x, 0.5, tween_function=ease_out_cubic, cb_end=wait_message)
            sm = self.system_message_info
            sm['text'] = text
            sm['sprite'] = spr
            sm['state'] = 1

        def wait_message(*args):
            spr, attr_name, tween = args
            spr.misc_effect('dummy', 0.0, 1.0, 2.0, cb_end=exit_message)
            spr.tween_callback_end(*args)
            sm = self.system_message_info
            sm['state'] = 2

        def exit_message(*args):
            spr, attr_name, tween = args
            text_half_w = spr.image.get_width() // 2
            text_middle_x = settings.screen_width // 2 + text_half_w
            text_right_x = settings.screen_width + text_half_w
            spr.driftx_effect(text_middle_x, text_right_x, 1.0, tween_function=ease_in_cubic, cb_end=end_message)
            spr.tween_callback_end(*args)
            sm = self.system_message_info
            sm['state'] = 3

        def end_message(*args):
            spr, attr_name, tween = args
            spr.tween_callback_end(*args)
            sm = self.system_message_info
            sm['text'] = None
            sm['sprite'] = None
            sm['state'] = 0
            text = settings.system_messages_queue.pop(0)
            settings.system_messages_used.append(text)

        spritefx.update_tweens(delta_time)

        state = self.system_message_info['state']
        if state == 0 and settings.system_messages_queue:
            text = settings.system_messages_queue[0]
            if not text:
                return
            start_message(text)

    def _check_loose(self):
        alive_canoes = [c for c in self.canoes if c.health > 0]
        if len(alive_canoes) == 0:
            logger.info("============== LOOSE!  =============")
            if self.result is None:
                self.result = False
                def _cb(*args):
                    logger.info("============== Repeat level!  =============")
                    self.pop()
                    self.push(Game(self.level_num, self.damage_factor))
                timer = Timer(scheduler=self.scheduler)
                timer.event_elapsed += _cb
                timer.start(4, False)


    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        fill_color = settings.default_fill_color
        self.renderer.draw(screen, self.cam, fill_color, False, interpolation_factor)

        sm = self.system_message_info['sprite']
        if sm:
            screen.blit(sm.image, sm.rect.topleft)

        self.hud.draw(screen)

        if self._debug_render:
            self._draw_debug(screen)

        for idx, c in enumerate(self.canoes):
            # health bar
            r = pygame.Rect(10, 10 + idx * 12, 100, 10)
            pygame.draw.rect(screen, (0, 255, 0), r, 1)
            r.w = c.health
            pygame.draw.rect(screen, (0, 255, 0), r)

            # air
            r = pygame.Rect(120, 10 + idx * 12, settings.max_under_water_time_span_in_seconds * 10, 10)
            pygame.draw.rect(screen, (0, 100, 255), r, 1)
            r.w = c.left_time_span_under_water * 10
            pygame.draw.rect(screen, (0, 100, 255), r)

        pygame.display.flip()

    def _draw_debug(self, screen):
        for idx, c in enumerate(self.canoes):
            color = (255, 255, 255)  # white
            if c.state_machine.current_state == Watered:
                color = (255, 0, 0)  # red
            elif c.state_machine.current_state == Flipped:
                color = (100, 0, 0)  # dark red
            elif c.state_machine.current_state == Airborne:
                color = (255, 255, 0)  # yellow

            pos = self.cam.world_to_screen(c.position * PPU)
            direction = Vec3(c.v.x, c.v.y).normalized

            # canoe shadow
            if c.state_machine.current_state != WaterfallFalling:
                front = pos + direction * settings.canoe_length / 2.0 * PPU
                back = pos - direction * settings.canoe_length / 2.0 * PPU
                pygame.draw.line(screen, (100, 100, 100), front.as_xy_tuple(int), back.as_xy_tuple(int), 3)

            # canoe body
            pos.y += c.position.z * PPU
            front = pos + direction * settings.canoe_length / 2.0 * PPU
            back = pos - direction * settings.canoe_length / 2.0 * PPU
            pygame.draw.line(screen, color, front.as_xy_tuple(int), back.as_xy_tuple(int), 3)

            # canoe velocity
            pygame.draw.line(screen, (0, 255, 0), pos.as_xy_tuple(int), (pos + c.v * PPU).as_xy_tuple(int))

            # canoe collision box
            r = pygame.Rect(0, 0, settings.canoe_collision_box_size, settings.canoe_collision_box_size)
            r.center = pos.as_xy_tuple(int)
            pygame.draw.rect(screen, (0, 255, 0), r, 1)

            # border
            r.center = (c.position * PPU).as_xy_tuple(int)
            for border in self.level.space.get_in_rect(*tuple(r.inflate(1000, 1000))):
                # if border.kind == settings.KIND_WALL_EDGE or border.kind == settings.KIND_LOG_EDGE:
                if isinstance(border, walledge.EdgeEntity):
                    p0 = self.cam.world_to_screen(border.p0).as_xy_tuple(int)
                    p1 = self.cam.world_to_screen(border.p1).as_xy_tuple(int)
                    pygame.draw.line(screen, (178, 0, 255), p0, p1, 1)
                    pm = self.cam.world_to_screen(border.position)
                    pn = self.cam.world_to_screen(border.position + border.normal * 10)
                    pygame.draw.line(screen, (200, 0, 255), pm.as_xy_tuple(int), pn.as_xy_tuple(int), 1)
                elif border.kind == KIND_GATOR:
                    p = self.cam.world_to_screen(border.position).as_xy_tuple(int)
                    direction = self.cam.world_to_screen(border.position + border.direction * 10).as_xy_tuple(int)
                    pygame.draw.line(screen, (255, 255, 0), p, direction)

                # draw rect
                x, y, w, h = border.aabb
                sx, sy = self.cam.world_to_screen(Point(x, y)).as_xy_tuple(int)
                pygame.draw.rect(screen, (150, 150, 150), (sx, sy, w, h), 1)




logger.debug("imported")
