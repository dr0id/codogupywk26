# -*- coding: utf-8 -*-
import logging
from pyknic.generators import IdGenerator
from gamelib import settings
from gamelib.sfx import MusicData, SoundData


logger = logging.getLogger(__name__)


def play_sound(idx, queue=False):
    settings.sfx_handler.handle_message(None, None, idx, 'queue' if queue else None)


def play_song(idx):
    settings.sfx_handler.handle_message(None, None, idx, None)


_gen_id = IdGenerator(1).next

SFX_PLAYER_JUMP = _gen_id()
SFX_PLAYER_FLIP = _gen_id()
SFX_PLAYER_SUBMERGE = _gen_id()
SFX_PLAYER_SPLAT = _gen_id()

SFX_LOSE_LEVEL = _gen_id()
SFX_WIN_LEVEL = _gen_id()

SFX_PADDLE_1 = _gen_id()
SFX_PADDLE_2 = _gen_id()
SFX_PADDLE_3 = _gen_id()
SFX_PADDLE_4 = _gen_id()
SFX_PADDLE_5 = _gen_id()

SFX_WATERFALL = _gen_id()
SFX_WATERFALL_CRASH = _gen_id()
SFX_RAPIDS = _gen_id()

SFX_GATOR_GROWL_1 = _gen_id()
SFX_GATOR_GROWL_2 = _gen_id()
SFX_GATOR_GROWL_3 = _gen_id()

SFX_GATOR_SPLASH_1 = _gen_id()
SFX_GATOR_SPLASH_2 = _gen_id()
SFX_GATOR_SPLASH_3 = _gen_id()
SFX_GATOR_SPLASH_4 = _gen_id()
SFX_GATOR_SPLASH_5 = _gen_id()

SFX_HIT_1 = _gen_id()
SFX_HIT_2 = _gen_id()
SFX_HIT_3 = _gen_id()
SFX_CINEMATIC_HIT_1 = _gen_id()
SFX_CINEMATIC_HIT_2 = _gen_id()
SFX_CINEMATIC_HIT_3 = _gen_id()
SFX_CINEMATIC_HIT_4 = _gen_id()
SFX_CINEMATIC_HIT_5 = _gen_id()
SFX_CINEMATIC_HIT_6 = _gen_id()
SFX_CINEMATIC_HIT_7 = _gen_id()
SFX_CINEMATIC_HIT_8 = _gen_id()

SFX_PADDLES = (
    SFX_PADDLE_1,
    SFX_PADDLE_2,
    SFX_PADDLE_3,
    SFX_PADDLE_4,
    SFX_PADDLE_5,
)
SFX_GATOR_GROWLS = (
    SFX_GATOR_GROWL_1,
    SFX_GATOR_GROWL_2,
    SFX_GATOR_GROWL_3,
)
SFX_GATOR_SPLASHES = (
    SFX_GATOR_SPLASH_1,
    SFX_GATOR_SPLASH_2,
    SFX_GATOR_SPLASH_3,
    SFX_GATOR_SPLASH_4,
    SFX_GATOR_SPLASH_5,
)
SFX_HITS = (
    SFX_HIT_1,
    SFX_HIT_2,
    SFX_HIT_3,
)
SFX_CINEMATIC_HITS = (
    SFX_CINEMATIC_HIT_1,
    SFX_CINEMATIC_HIT_2,
    SFX_CINEMATIC_HIT_3,
    SFX_CINEMATIC_HIT_4,
    SFX_CINEMATIC_HIT_5,
    SFX_CINEMATIC_HIT_6,
    SFX_CINEMATIC_HIT_7,
    SFX_CINEMATIC_HIT_8,
)

SONG_1 = _gen_id()
SONG_2 = _gen_id()
SONG_3 = _gen_id()
SONG_4 = _gen_id()

sfx_data = {
    # SFX_PLAYER_JUMP: SoundData('NYX_JUMP4.ogg', 0.5, reserved_channel_id=settings.channel_jumping),
    # SFX_PLAYER_LAND: SoundData('NYX_LAND3.ogg', 0.5, reserved_channel_id=settings.channel_jumping),
    # SFX_x1: SoundData('boing1.ogg', 0.3, reserved_channel_id=settings.channel_trampoline),
    # SFX_x2: SoundData('boing2.ogg', 0.3, reserved_channel_id=settings.channel_trampoline),
    SFX_PLAYER_JUMP: SoundData('jump.ogg', 1.0 * settings.master_volume),
    SFX_PLAYER_SPLAT: SoundData('splat.ogg', 1.0 * settings.master_volume),
    SFX_PLAYER_SUBMERGE: SoundData('submerge.ogg', 1.0 * settings.master_volume),
    SFX_WATERFALL_CRASH: SoundData('waterfall crash.ogg', 1.0 * settings.master_volume),
    SFX_WATERFALL: SoundData('waterfall.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_waterfall),
    SFX_LOSE_LEVEL: SoundData('lose level.ogg', 1.0 * settings.master_volume),
    SFX_WIN_LEVEL: SoundData('win level.ogg', 1.0 * settings.master_volume),

    SFX_GATOR_GROWL_1: SoundData('Alligator Growl 1.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_gator_growl),
    SFX_GATOR_GROWL_2: SoundData('Alligator Growl 2.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_gator_growl),
    SFX_GATOR_GROWL_3: SoundData('Alligator Growl 3.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_gator_growl),

    SFX_HIT_1: SoundData('Body_Punch_1.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_hit),
    SFX_HIT_2: SoundData('Body_Punch_2.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_hit),
    SFX_HIT_3: SoundData('Body_Punch_3.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_hit),

    SFX_CINEMATIC_HIT_1: SoundData('Cinematic Hit Sound Effects 1.ogg', 1.0 * settings.master_volume),
    SFX_CINEMATIC_HIT_2: SoundData('Cinematic Hit Sound Effects 2.ogg', 1.0 * settings.master_volume),
    SFX_CINEMATIC_HIT_3: SoundData('Cinematic Hit Sound Effects 3.ogg', 1.0 * settings.master_volume),
    SFX_CINEMATIC_HIT_4: SoundData('Cinematic Hit Sound Effects 4.ogg', 1.0 * settings.master_volume),
    SFX_CINEMATIC_HIT_5: SoundData('Cinematic Hit Sound Effects 5.ogg', 1.0 * settings.master_volume),
    SFX_CINEMATIC_HIT_6: SoundData('Cinematic Hit Sound Effects 6.ogg', 1.0 * settings.master_volume),
    SFX_CINEMATIC_HIT_7: SoundData('Cinematic Hit Sound Effects 7.ogg', 1.0 * settings.master_volume),
    SFX_CINEMATIC_HIT_8: SoundData('Cinematic Hit Sound Effects 8.ogg', 1.0 * settings.master_volume),

    SFX_GATOR_SPLASH_1: SoundData('gator splash 1.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_splashes),
    SFX_GATOR_SPLASH_2: SoundData('gator splash 2.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_splashes),
    SFX_GATOR_SPLASH_3: SoundData('gator splash 3.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_splashes),
    SFX_GATOR_SPLASH_4: SoundData('gator splash 4.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_splashes),
    SFX_GATOR_SPLASH_5: SoundData('gator splash 5.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_splashes),

    SFX_PADDLE_1: SoundData('paddle 1.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_paddle),
    SFX_PADDLE_2: SoundData('paddle 2.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_paddle),
    SFX_PADDLE_3: SoundData('paddle 3.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_paddle),
    SFX_PADDLE_4: SoundData('paddle 4.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_paddle),
    SFX_PADDLE_5: SoundData('paddle 5.ogg', 1.0 * settings.master_volume, reserved_channel_id=settings.channel_paddle),

    # SONG_x: MusicData('Factory - Remember.ogg', 1.0),
    SONG_1: MusicData('Airwolf Theme - Banjo cover.ogg', 1.0 * settings.master_volume),
    SONG_2: MusicData('Dune music cover - Sign Of The Worm #dune.ogg', 1.0 * settings.master_volume),
    SONG_3: MusicData('Journey to Silius - Level 2 theme BANJO cover.ogg', 1.0 * settings.master_volume),
    SONG_4: MusicData('Knight Rider Intro theme BANJO cover.ogg', 1.0 * settings.master_volume),
}
