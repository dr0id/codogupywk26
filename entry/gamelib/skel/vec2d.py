# -*- coding: utf-8 -*-
# Public domain. No warranty.

"""Vec2d.py - Nice implementation of a 2D vector coordinate.

Source: http://www.pygame.org/wiki/2DVectorClass

Converted to subclass of list for speed. Much faster.
"""

import operator
import math


__all__ = ['Vec2d']


class Vec2d(list):
    __slots__ = []

    def __init__(self, x_or_pair, y=None):
        # super(Vec2d, self).__init__((x, y))
        if y is None:
            self.extend(x_or_pair)
        else:
            append = self.append
            append(x_or_pair)
            append(y)

    def get_x(self):
        return self[0]

    def set_x(self, value):
        self[0] = value
    x = property(get_x, set_x)

    def get_y(self):
        return self[1]

    def set_y(self, value):
        self[1] = value
    y = property(get_y, set_y)

    def __len__(self):
        return 2

    # String representaion (for debugging)
    def __repr__(self):
        return 'Vec2d(%s, %s)' % (self[0], self[1])

    # Comparison
    def __eq__(self, other):
        try:
            return self[0] == other[0] and self[1] == other[1] and len(other) == 2
        except:
            return False

    def __ne__(self, other):
        try:
            return self[0] != other[0] or self[1] != other[1] or len(other) != 2
        except:
            return True

    def __nonzero__(self):
        # Python 2.7 - see __bool__ for Python 3
        return bool(self[0] or self[1])

    def __bool__(self):
        # Python 3 - see __nonzero__ for Python 2.7
        return bool(self[0] or self[1])

    # Addition
    def __add__(self, other):
        # raise Exception
        try:
            x, y = other[0:2]
            return Vec2d(self[0] + x, self[1] + y)
        except:
            return Vec2d(self[0] + other, self[1] + other)
    __radd__ = __add__

    def __iadd__(self, other):
        try:
            self[0] += other[0]
            self[1] += other[1]
        except:
            self[0] += other
            self[1] += other
        return self

    # Subtraction
    def __sub__(self, other):
        try:
            return Vec2d(self[0] - other[0], self[1] - other[1])
        except:
            return Vec2d(self[0] - other, self[1] - other)

    def __rsub__(self, other):
        try:
            return Vec2d(other[0] - self[0], other[1] - self[1])
        except:
            return Vec2d(other - self[0], other - self[1])

    def __isub__(self, other):
        try:
            self[0] -= other[0]
            self[1] -= other[1]
        except:
            self[0] -= other
            self[1] -= other
        return self

    # Multiplication
    def __mul__(self, other):
        try:
            return Vec2d(self[0] * other[0], self[1] * other[1])
        except:
            return Vec2d(self[0] * other, self[1] * other)
    __rmul__ = __mul__

    def __imul__(self, other):
        try:
            self[0] *= other[0]
            self[1] *= other[1]
        except:
            self[0] *= other
            self[1] *= other
        return self

    # Division
    def __div__(self, other):
        return self._o2(other, operator.div)

    def __rdiv__(self, other):
        return self._r_o2(other, operator.div)

    def __idiv__(self, other):
        return self._io(other, operator.div)

    def __floordiv__(self, other):
        return self._o2(other, operator.floordiv)

    def __rfloordiv__(self, other):
        return self._r_o2(other, operator.floordiv)

    def __ifloordiv__(self, other):
        return self._io(other, operator.floordiv)

    def __truediv__(self, other):
        return self._o2(other, operator.truediv)

    def __rtruediv__(self, other):
        return self._r_o2(other, operator.truediv)

    def __itruediv__(self, other):
        return self._io(other, operator.floordiv)

    # Modulo
    def __mod__(self, other):
        return self._o2(other, operator.mod)

    def __rmod__(self, other):
        return self._r_o2(other, operator.mod)

    def __divmod__(self, other):
        return self._o2(other, operator.divmod)

    def __rdivmod__(self, other):
        return self._r_o2(other, operator.divmod)

    def _o2(self, other, f):
        """Any two-operator operation where the left operand is a Vec2d"""
        try:
            return Vec2d(f(self[0], other[0]), f(self[1], other[1]))
        except:
            return Vec2d(f(self[0], other), f(self[1], other))

    def _r_o2(self, other, f):
        """Any two-operator operation where the right operand is a Vec2d"""
        try:
            return Vec2d(f(other[0], self[0]), f(other[1], self[1]))
        except:
            return Vec2d(f(other, self[0]), f(other, self[1]))

    def _io(self, other, f):
        """inplace operator"""
        try:
            self[0] = f(self[0], other[0])
            self[1] = f(self[1], other[1])
        except:
            self[0] = f(self[0], other)
            self[1] = f(self[1], other)
        return self

    # Exponentation
    def __pow__(self, other):
        return self._o2(other, operator.pow)

    def __rpow__(self, other):
        return self._r_o2(other, operator.pow)

    # Bitwise operators
    def __lshift__(self, other):
        return self._o2(other, operator.lshift)

    def __rlshift__(self, other):
        return self._r_o2(other, operator.lshift)

    def __rshift__(self, other):
        return self._o2(other, operator.rshift)

    def __rrshift__(self, other):
        return self._r_o2(other, operator.rshift)

    def __and__(self, other):
        return self._o2(other, operator.and_)
    __rand__ = __and__

    def __or__(self, other):
        return self._o2(other, operator.or_)
    __ror__ = __or__

    def __xor__(self, other):
        return self._o2(other, operator.xor)
    __rxor__ = __xor__

    # Unary operations
    def __neg__(self):
        return Vec2d(operator.neg(self[0]), operator.neg(self[1]))

    def __pos__(self):
        return Vec2d(operator.pos(self[0]), operator.pos(self[1]))

    def __abs__(self):
        return Vec2d(abs(self[0]), abs(self[1]))

    def __invert__(self):
        return Vec2d(-self[0], -self[1])

    # vectory functions
    def get_length_sqrd(self):
        return self[0] * self[0] + self[1] * self[1]

    def get_length(self):
        return math.sqrt(self[0] * self[0] + self[1] * self[1])

    def __setlength(self, value):
        length = self.get_length()
        self[0] *= value / length
        self[1] *= value / length
    length = property(get_length, __setlength, None, "gets or sets the magnitude of the vector")

    def rotate(self, angle_degrees):
        radians = math.radians(angle_degrees)
        cos = math.cos(radians)
        sin = math.sin(radians)
        x = self[0] * cos - self[1] * sin
        y = self[0] * sin + self[1] * cos
        self[0] = x
        self[1] = y

    def rotated(self, angle_degrees):
        radians = math.radians(angle_degrees)
        cos = math.cos(radians)
        sin = math.sin(radians)
        x = self[0] * cos - self[1] * sin
        y = self[0] * sin + self[1] * cos
        return Vec2d(x, y)

    def get_angle(self):
        if self.get_length_sqrd() == 0:
            return 0
        return math.degrees(math.atan2(self[1], self[0]))

    def __setangle(self, angle_degrees):
        self[0] = self.length
        self[1] = 0
        self.rotate(angle_degrees)
    angle = property(get_angle, __setangle, None, "gets or sets the angle of a vector")

    def get_angle_between(self, other):
        cross = self[0] * other[1] - self[1] * other[0]
        dot = self[0] * other[0] + self[1] * other[1]
        return math.degrees(math.atan2(cross, dot))

    def normalized(self):
        length = self.length
        if length != 0:
            return self / length
        return Vec2d(self)

    def normalize_return_length(self):
        length = self.length
        if length != 0:
            self[0] /= length
            self[1] /= length
        return length

    def perpendicular(self):
        return Vec2d(-self[1], self[0])

    def perpendicular_normal(self):
        length = self.length
        if length != 0:
            return Vec2d(-self[1] / length, self[0] / length)
        return Vec2d(self)

    def dot(self, other):
        return float(self[0] * other[0] + self[1] * other[1])

    def get_distance(self, other):
        a = self[0] - other[0]
        b = self[1] - other[1]
        return math.sqrt(a * a + b * b)

    def get_dist_sqrd(self, other):
        a = self[0] - other[0]
        b = self[1] - other[1]
        return a * a + b * b

    def projection(self, other):
        other_length_sqrd = other[0] * other[0] + other[1] * other[1]
        projected_length_times_other_length = self.dot(other)
        return other * (projected_length_times_other_length / other_length_sqrd)

    def cross(self, other):
        return self[0] * other[1] - self[1] * other[0]

    def interpolate_to(self, other, range):
        return Vec2d(self[0] + (other[0] - self[0]) * range, self[1] + (other[1] - self[1]) * range)

    def convert_to_basis(self, x_vector, y_vector):
        return Vec2d(self.dot(x_vector) / x_vector.get_length_sqrd(), self.dot(y_vector) / y_vector.get_length_sqrd())

    def __getstate__(self):
        return [self[0], self[1]]

    def __setstate__(self, dict):
        self[0], self[1] = dict


########################################################################
## Unit Testing                                                       ##
########################################################################
if __name__ == "__main__":

    import unittest
    import pickle

    ####################################################################
    class UnitTestVec2D(unittest.TestCase):

        def setUp(self):
            pass

        def testCreationAndAccess(self):
            v = Vec2d(111,222)
            self.assertTrue(v.x == 111 and v.y == 222)
            v.x = 333
            v[1] = 444
            self.assertTrue(v[0] == 333 and v[1] == 444)

        def testMath(self):
            v = Vec2d(111,222)
            self.assertEqual(v + 1, Vec2d(112, 223))
            self.assertTrue(v - 2 == [109, 220])
            self.assertTrue(v * 3 == (333, 666))
            self.assertTrue(v / 2.0 == Vec2d(55.5, 111))
            self.assertTrue(v // 2 == (55, 111))
            self.assertTrue(v ** Vec2d(2, 3) == [12321, 10941048])
            self.assertTrue(v + [-11, 78] == Vec2d(100, 300))
            self.assertTrue(v // [11, 2] == [10, 111])

        def testReverseMath(self):
            v = Vec2d(111, 222)
            self.assertTrue(1 + v == Vec2d(112, 223))
            self.assertTrue(2 - v == [-109, -220])
            self.assertTrue(3 * v == (333, 666))
            self.assertTrue([222, 999] // v == [2, 4])
            self.assertTrue([111, 222] ** Vec2d(2, 3) == [12321, 10941048])
            self.assertTrue([-11, 78] + v == Vec2d(100, 300))

        def testUnary(self):
            v = Vec2d(111, 222)
            v = -v
            self.assertTrue(v == [-111, -222])
            v = abs(v)
            self.assertTrue(v == [111, 222])

        def testLength(self):
            v = Vec2d(3, 4)
            self.assertTrue(v.length == 5)
            self.assertTrue(v.get_length_sqrd() == 25)
            self.assertTrue(v.normalize_return_length() == 5)
            self.assertTrue(v.length == 1)
            v.length = 5
            self.assertTrue(v == Vec2d(3, 4))
            v2 = Vec2d(10, -2)
            self.assertTrue(v.get_distance(v2) == (v - v2).get_length())

        def testAngles(self):
            v = Vec2d(0, 3)
            self.assertEqual(v.angle, 90)
            v2 = Vec2d(v)
            v.rotate(-90)
            self.assertEqual(v.get_angle_between(v2), 90)
            v2.angle -= 90
            self.assertEqual(v.length, v2.length)
            self.assertEqual(v2.angle, 0)
            self.assertEqual(v2, [3, 0])
            self.assertTrue((v - v2).length < .00001)
            self.assertEqual(v.length, v2.length)
            v2.rotate(300)
            self.assertAlmostEqual(v.get_angle_between(v2), -60)
            v2.rotate(v2.get_angle_between(v))
            angle = v.get_angle_between(v2)
            self.assertAlmostEqual(v.get_angle_between(v2), 0)

        def testHighLevel(self):
            basis0 = Vec2d(5.0, 0)
            basis1 = Vec2d(0, .5)
            v = Vec2d(10, 1)
            self.assertTrue(v.convert_to_basis(basis0, basis1) == [2, 2])
            self.assertTrue(v.projection(basis0) == (10, 0))
            self.assertTrue(basis0.dot(basis1) == 0)

        def testCross(self):
            lhs = Vec2d(1, .5)
            rhs = Vec2d(4, 6)
            self.assertTrue(lhs.cross(rhs) == 4)

        def testComparison(self):
            int_vec = Vec2d(3, -2)
            flt_vec = Vec2d(3.0, -2.0)
            zero_vec = Vec2d(0, 0)
            self.assertTrue(int_vec == flt_vec)
            self.assertTrue(int_vec != zero_vec)
            self.assertTrue((flt_vec == zero_vec) == False)
            self.assertTrue((flt_vec != int_vec) == False)
            self.assertTrue(int_vec == (3, -2))
            self.assertTrue(int_vec != [0, 0])
            self.assertTrue(int_vec != 5)
            self.assertTrue(int_vec != [3, -2, -5])

        def testInplace(self):
            inplace_vec = Vec2d(5, 13)
            inplace_ref = inplace_vec
            inplace_src = Vec2d(inplace_vec)
            inplace_vec *= .5
            inplace_vec += .5
            inplace_vec //= (3, 6)
            inplace_vec += Vec2d(-1, -1)
            alternate = (inplace_src * .5 + .5) // Vec2d(3, 6) + [-1, -1]
            self.assertEqual(inplace_vec, inplace_ref)
            self.assertEqual(inplace_vec, alternate)

        def testPickle(self):
            testvec = Vec2d(5, .3)
            testvec_str = pickle.dumps(testvec)
            loaded_vec = pickle.loads(testvec_str)
            self.assertEqual(testvec, loaded_vec)

        p = Vec2d(1, 2)
        print(p[:])

    ####################################################################
    unittest.main()

    ########################################################################
