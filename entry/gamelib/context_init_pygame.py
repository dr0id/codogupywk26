# -*- coding: utf-8 -*-
import logging
import os

import pygame

from gamelib import settings
from gamelib import resource_sound
from pyknic import context

logger = logging.getLogger(__name__)


class PygameInitContext(context.Context):

    def __init__(self):
        context.Context.__init__(self)

    def enter(self):
        self.init_pygame()

    def exit(self):
        try:
            pygame.mixer.music.fadeout(2000)
            while pygame.mixer.music.get_busy():
                pygame.time.wait(100)
        except IndexError:
            pass

        pygame.quit()

    def update(self, delta_time, sim_time):
        self.pop()

    def init_pygame(self):
        logger.info("%s pygame init %s", "#" * 10, "#" * 10)
        os.environ['SDL_VIDEO_CENTERED'] = '1'
        pygame.mixer.pre_init(22050, -16, 2, 64)
        pygame.mixer.pre_init(frequency=settings.MIXER_FREQUENCY, buffer=settings.MIXER_BUFFER_SIZE)
        # num_pass, num_fail = pygame.init()
        # logger.info("pygame.init(): pass: %s  failed: %s", num_pass, num_fail)
        # if num_fail != 0:
        #     logger.error("pygame failed init pass:%s failed:%s", num_pass, num_fail)
        #
        # if num_pass <= 0:
        #     logger.error("pygame didn't initialize anything!")
        import pkgutil
        import importlib

        for module_info in pkgutil.walk_packages(pygame.__path__, "pygame."):
            # logger.info("!! %s", module_info)
            try:
                _finder, name, is_pkg = module_info
                if "examples" in name \
                        or "tests" in name \
                        or "docs" in name \
                        or "threads" in name \
                        or "scrap" in name \
                        :
                    continue  # skip
                logger.info("loading module: %s", name)
                _module = importlib.import_module(name)
                if hasattr(_module, 'init'):
                    self._init_module(_module)
                else:
                    logger.info("  module has no init(): %s", _module)
            except ImportError as mnfe:
                logger.warning(mnfe)
            except AttributeError as ae:
                logger.error(ae)

        del pkgutil
        del importlib

        display_info = pygame.display.Info()
        # logger.info("display info: {0}", display_info)
        csh = display_info.current_h
        if int(csh * 0.9) < settings.screen_height:
            logger.info("accommodating height to {0}", csh * 0.9)
            settings.screen_height = int(csh * 0.9)  # accommodate for title bar and task bar ?
        settings.screen_size = settings.screen_width, settings.screen_height
        logger.info("using screen size: {0}", settings.screen_size)

        settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

        pygame.mixer.init()
        pygame.mixer.set_num_channels(settings.MIXER_NUM_CHANNELS)
        pygame.mixer.set_reserved(settings.MIXER_RESERVED_CHANNELS)
        settings.sfx_handler.load(resource_sound.sfx_data)
        settings.sfx_handler.fill_music_carousel((
            resource_sound.SONG_1,
            resource_sound.SONG_4,
            resource_sound.SONG_3,
            resource_sound.SONG_2,
        ))

        # scrap has to come after display init
        try:
            logger.info("loading module: %s", pygame.scrap)
            self._init_module(pygame.scrap)
        except AttributeError as ae:
            logger.warning(ae)

        # icon = pygame.image.load("./data/gfx/misc/icon.png")
        # pygame.display.set_icon(icon)
        try:
            icon = pygame.image.load(settings.path_to_icon)
            pygame.display.set_icon(icon)
        except pygame.error as pger:
            logger.error("could not find icon.png in %s: error: %s", os.path.abspath(settings.path_to_icon), str(pger))

        pygame.display.set_caption(settings.title)

        settings.screen.fill(settings.default_fill_color)
        pygame.display.flip()
        settings.sfx_handler.start_music_carousel()
        logger.info("%s pygame init DONE %s", "#" * 10, "#" * 10)

    def _init_module(self, m):
        logger.info("  init() %s", m)
        m.init()
        try:
            if m.get_init():
                logger.info("    get_init() True %s", m)
            else:
                logger.error("    get_init() False %s", m)
        except AttributeError as ae:
            # module has not get_init() method
            logger.error(ae)
