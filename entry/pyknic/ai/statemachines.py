# -*- coding: utf-8 -*-

"""
This module contains different state machine implementations.


A sample state transition::

        +---------------+                                       +---------------+
        | state1        |                                       | state2        |
        |               |                                       |               |
        | /entry        |            event / action             | /entry        |
        | /exit         |-------------------------------------->| /exit         |
        | /activate     |                                       | /activate     |
        | /deactivate   |                                       | /deactivate   |
        |               |                                       |               |
        | +update       |                                       | +update       |
        |               |                                       |               |
        +---------------+                                       +---------------+


Sequence diagram of a transition (with optional action)::

                           state machine                  state1                      state2
                                |                           |                           |
                handle_event    |        handle_event       |                           |
                    ----------->|-------------------------->|                           |
                                |        switch_state       |                           |
                                |<--------------------------|                           |
                                |        exit               |                           |
                                |-------------------------->|                           |
        event_switching_state   |                           |                           |
                    <-----------|                           |                           |
                action          |                           |                           |
                    <-----------|                           |                           |
                                |        enter              |                           |
                                |------------------------------------------------------>|
        event_switched_state    |                           |                           |
                    <-----------|                           |                           |
                                |        switch_state       |                           |
                                |-  -  -  -  -  -  -  -  - >|                           |
                handle_event    |        handle_event       |                           |
                    < -  -  -  -|<  -  -  -  -  -  -  -  -  |                           |
                                |                           |                           |
                                |                           |                           |


Looking at the code, you will miss the handle_event method. This is intentional since this code is thought to be a
base to implement state driven agent design. One can implement it in different ways.

The important thing is that the state machine only handles events and that each state decides or contains the logic
to switch to another state. The handle_event method is called by the surrounding code, the environment. Its the
method through which the interaction with the agent from outside is performed.

Example 1 is compact and all the switching logic is in the handle_event method of the
states. The Example 2 the events are handled through dedicated methods.

Example 1::

    class Agent1(object):

        def __init__(self):
            self.state_machine = SimpleStateMachine(InitialState)
            # ...

        def handle_event(self, event):
            # the state switching logic is encapsulated in the states itself
            self.state_machine.current_state.handle_event(self.state_machine, event)

Example 2::

    class Agent2(object):

        def __init__(self):
            self.state_machine = SimpleStateMachine(InitialState)
            # ...

        def event1(self):
            self.state_machine.current_state.event1(self.state_machine)

        def event2(self):
            self.state_machine.current_state.event2(self.state_machine)

        # .... and so on, each event is handled through a method

A not recommended way of implementing the state_machine is in Example 3. The drawback there is that the
state switching logic is encapsulated in the if ...(elif)...else constructs in the event handling methods and not in
in the state itself (this is why the state has to be checked, which is bad).

Example 3::

    class Agent3(object):

        def __init__(self):
            self.state_machine = SimpleStateMachine(InitialState)
            # ...

        def event1(self):
            self.state_machine.switch_state(StateA)

        def event2(self):
            if self.state_machine.current_state is StateA:
                self.state_machine.switch_state(StateB)
            if self.state_machine.current_state is StateX:
                self.state_machine.switch_state(StateB)
            # ...
            else:
                self.state_machine.switch_state(StateC

        # .... and so on, each event is handled through a method and the event switching logic is all here


"""
from __future__ import print_function, division

import logging

from pyknic import events

logger = logging.getLogger(__name__)
logger.debug("importing...")


class SwitchingStateException(Exception):
    pass


class BaseState(object):
    """
    This is a possible base state class. Inherit and override the methods.
    """

    @staticmethod
    def enter(owner):
        pass

    @staticmethod
    def exit(owner):
        pass

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        pass

    @staticmethod
    def handle_event(owner, event, *args):
        pass


class SimpleStateMachine(object):
    """
    A simple StateMachine.
    
    Usage::

        sm = StateMachine(InitialState)
        #sm.switch_state(State1) # set the first state
        
        # in main loop
        sm.current_state.update(dt)
        

        Never ever set current_state directly, ALWAYS use switch_state

    """

    def __init__(self, initial_state, name=None, previous_state=None, global_state=None, owner=None):
        """
        Constructor.
        :param initial_state: The initial state of the state machine.
        :param name: An optional name for this state machine used to identify this instance. Otherwise id(self) is used.
        :param previous_state: The previous state. If not set then initial_state is used.
        :param global_state: The global state to use. Its update method is called before update of the current state.
        :param owner: The owner of the state machine. If None then owner is set to self.
        """
        self.name = str(id(self)) if name is None else name
        self.owner = self if owner is None else owner
        self.event_switched_state = events.Signal("SimpleStateMachine.event_switched_state" + self.name)
        self.event_switching_state = events.Signal("SimpleStateMachine.event_switching_state" + self.name)
        self._switching_state = False
        self.current_state = initial_state
        self.previous_state = previous_state if previous_state is not None else initial_state
        self.global_state = global_state

    def switch_state(self, new_state, action=None):
        """
        Change to the next state. First exit is called on the current state,
        then the new state is made the current state and finally enter is called
        on the new state.

        During this method execution two events will be fired:
        event_switching_state with the arguments (state_machine, previous_state, new_state)
        This event is fired after current_state.exit and before action (if provided) and new_state.enter is called.

        The other event is fired after the switching is done.
        event_switched_state with the arguments (state_machine, previous_state).
        This event is fired after new_state.enter has been called.
        
        :Parameters:
            new_state : object
                Can be any object that has following methods: enter(sm) and 
                exit(sm), can not be None (use a end state instead).
            action : function
                A callback to be called as transition action. It will be called after the event_state_switching event
                and has only the state machine reference as argument, e.g. action(state_machine)
        
        """
        assert new_state is not None
        assert hasattr(new_state, "enter"), "new state should have an enter method"
        assert hasattr(getattr(new_state, "enter"), '__call__'), "new states enter attribute should be callable"
        assert hasattr(new_state, "exit"), "new state should have an exit method"
        assert hasattr(getattr(new_state, "exit"), '__call__'), "new states exit attribute should be callable"

        if self._switching_state is True:
            msg = "state machine is switching state, try to switch the state after (e.g. in the update " \
                  "method instead of enter or exit)!"
            raise SwitchingStateException(msg)

        self._switching_state = True

        self.current_state.exit(self.owner)
        self.previous_state = self.current_state
        self.current_state = new_state

        if self.event_switching_state.has_observers:  # avoid extra method call for performance
            self.event_switching_state.fire(self.owner, self.previous_state, new_state, self)
        if action is not None:
            action(self.owner)

        self.current_state.enter(self.owner)
        if self.event_switched_state.has_observers:  # avoid extra method call for performance
            self.event_switched_state.fire(self.owner, self.previous_state, self)

        self._switching_state = False

    def revert_to_previous_state(self):
        """
        Reverts the state machine to the previous state (actually it does self.switch_state(self.previous_state) )
        """
        self.switch_state(self.previous_state)

    def update(self, dt, sim_t, *args, **kwargs):
        if self.global_state is not None:
            self.global_state.update(self.owner, dt, sim_t, *args, **kwargs)
        self.current_state.update(self.owner, dt, sim_t, *args, **kwargs)


class StateDrivenAgentBase(object):
    """
    A base class for a state driven agent. It is already working, but normally it should be inherited to extend
    the interaction parts or other convenience methods.
    """

    def __init__(self, initial_state, owner=None):
        """
        The initial data needed.
        :param initial_state: The initial state. The enter method is not called. If needed, do it in your code.
        :param owner: The owner of this agent. If it is None then self is used.
        """
        owner = self if owner is None else owner
        self.state_machine = SimpleStateMachine(initial_state, owner=owner)

    def handle_event(self, event, *args):
        """
        Handle a state machine event.
        :param event: The event to handle.
        :return: None
        """
        self.state_machine.current_state.handle_event(self.state_machine.owner, event, *args)

    def update(self, dt, sim_t, *args, **kwargs):
        """
        Updates the state of the agent and internal state machine.
        :param dt: delta time.
        :param sim_t: simulation time.
        :param args: `*args`
        :param kwargs: `**kwargs`
        :return: None
        """
        self.state_machine.update(dt, sim_t, *args, **kwargs)

    def handle_message(self, sender, receiver, msg_type, extra):
        """
        Handle the messages for this entity. Overwrite in derived class.
        :param sender: The sender id.
        :param receiver: The receiver id.
        :param msg_type: The message type.
        :param extra: Extra info for that msg type.
        :return: True to notify sender that you accepted, otherwise False.
        """
        pass

logger.debug("imported")
