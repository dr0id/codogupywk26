# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'eventmapper.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains code for mapping an pygame event to an action id. The idea is that your code only uses
actions as input and is independent of the pygame classes.


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

import pygame

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# list of public visible parts of this module
__all__ = ["NO_MOD", "ANY_MOD", "ANY_KEY", "EventMapper", "EventMapValidationError"]

logger = logging.getLogger(__name__)
logger.debug("importing...")

#: The constant used to indicate that no mod key should be pressed. Be warned about CapsLock, Shift and NumPad
NO_MOD = pygame.KMOD_NONE

#: The constant used to indicate the any mod can be pressed.
ANY_MOD = 0xFFFF

#: The constant that no key may be pressed.
NO_KEY = None

#: The constant used to indicate that any key can be pressed.
ANY_KEY = 0xFFFF


class EventMapValidationError(Exception):
    """The validation error for an event map."""
    pass


class EventMapper(object):
    """
    The event mapper class.

    """

    def __init__(self, even_to_action_map=None):
        """
        The EventMapper.

        :param even_to_action_map: The mapping between events and actions. See set_event_map method for full description
        """
        self._seen_unmapped = []
        self._event_to_action_map = {}
        if even_to_action_map is not None:
            self.set_event_map(even_to_action_map)

    def set_event_map(self, event_to_action_map):
        """
        Method for setting the event to action map.

        An event map has following structure::

            {
                event_type1 : {
                        filter_key1: action_id1,
                        filter_key2: action_id2,
                        },
                event_type2: {
                        filter_key3: action_id3,
                        },
            }

        The event_types define for which event and the filter_key define which event criteria triggers the action_id.
        Of course an action_id can be used in more than one event_type.

        Here an example::

            event_map = {
                pygame.KEYDOWN: {
                            (pygame.K_q, pygame.KMOD_CTRL): action_quit,
                            (pygame.K_F4, pygame.KMOD_ALT): action_quit,
                            }
                pygame.MOUSEBUTTONDOWN: {1: action_shoot, }
            }

        The map has two levels: the first level describes the event type. And in the second level a event specific
        property (filter_key) is used to map to an action. E.g. for a MOUSEBUTTONDOWN event (this is the first level,
        the event type) the second level map is about which button was pressed and which buttons maps to which action
        like {1: the_action}.


        There are following event_types that have different filter_keys to map:

         * pygame.MOUSEMOTION: {None | buttons: action_id} -> (action_id, (pos, rel, buttons))
         * pygame.MOUSEBUTTONDOWN: {button_id: action_id} -> (action_id, pos)
         * pygame.MOUSEBUTTONUP: {button_id: action_id} -> (action_id, pos)
         * pygame.KEYDOWN: {(key, mod) | scancode: action_id} -> (action_id, unicode)
         * pygame.KEYUP: {(key, mod) | scancode: action_id) -> (action_id, None)
         * pygame.JOYAXISMOTION: {(joy, axis): action_id} -> (action_id, value)
         * pygame.JOYBALLMOTION: {(joy, ball): action_id) -> (action_id, rel)
         * pygame.JOYHATMOTION: {(joy, hat): action_id} -> (action_id, value)
         * pygame.JOYBUTTONUP: {(joy, button_id): action_id} -> (action_id, None)
         * pygame.JOYBUTTONDOWN: {(joy, button_id): action_id} -> (action_id, None)
         * pygame.QUIT: {None: action_id} -> (action_id, None)
         * pygame.ACTIVEEVENT: {(state, gain): action_id} -> (action_id, None)
         * pygame.VIDEORESIZE: {None: action_id} -> (action_id, (w, h))
         * pygame.VIDEOEXPOSE: {None: action_id} -> (action_id, None)
         * pygame.USEREVENT: {('a', 2): action_id} -> (action_id, {'a': 1, 'b': 2})


        :param event_to_action_map:
        """
        if len(event_to_action_map) == 0:
            raise EventMapValidationError("event map should not be empty!")
        for _ev, _action_map in event_to_action_map.items():
            if len(_action_map) == 0:
                raise EventMapValidationError("empty action map detected for event {0}".format(_ev))

        self._event_to_action_map = event_to_action_map

    def get_actions(self, events_to_map):
        """
        usage::

            mapper = EventMapper(event_map)
            ...
            while running:
                events = pygame.event.get()
                actions, unmapped_events = mapper.get_actions(events)
                for action, extra in  actions:
                    ...

        :param events_to_map: list of pygame events
        :return: ([(action_id, extra), ], [unmapped_event, ])
        """
        result = []
        _unprocessed_events = []
        for event in events_to_map:
            event_type = event.type
            _actions = self._event_to_action_map.get(event_type, None)
            if _actions is None:
                _unprocessed_events.append(event)
                if event_type not in self._seen_unmapped:
                    self._seen_unmapped.append(event_type)
                    logger.debug("no actions found for event type: %s", event_type)
                continue

            _action_key = None
            _extra = None

            # MOUSEMOTION      pos, rel, buttons
            if event_type == pygame.MOUSEMOTION:
                # key = None
                buttons = tuple(event.buttons)
                if buttons in _actions.keys():
                    _action_key = buttons
                _extra = (event.pos, event.rel, event.buttons)

            # MOUSEBUTTONUP    pos, button
            elif event_type == pygame.MOUSEBUTTONDOWN:
                _action_key = event.button
                _extra = event.pos

            # MOUSEBUTTONDOWN  pos, button
            elif event_type == pygame.MOUSEBUTTONUP:
                _action_key = event.button
                _extra = event.pos

            # KEYDOWN          unicode, key, mod, scancode
            elif event_type == pygame.KEYDOWN:
                processed = False
                for _ak, _action_id in _actions.items():
                    processed |= self._process_key_event(event.key, event.mod, event.scancode, _ak, _action_id,
                                                         event.unicode, result)
                if processed:
                    continue

            # KEYUP            key, mod, scancode
            elif event_type == pygame.KEYUP:
                processed = False
                for _ak, _action_id in _actions.items():
                    processed |= self._process_key_event(event.key, event.mod, event.scancode, _ak, _action_id,
                                                         None, result)
                if processed:
                    continue

            # JOYAXISMOTION    joy, axis, value
            elif event_type == pygame.JOYAXISMOTION:
                _action_key = (event.joy, event.axis)
                _extra = event.value

            # JOYBALLMOTION    joy, ball, rel
            elif event_type == pygame.JOYBALLMOTION:
                _action_key = (event.joy, event.ball)
                _extra = event.rel

            # JOYHATMOTION     joy, hat, value
            elif event_type == pygame.JOYHATMOTION:
                _action_key = (event.joy, event.hat)
                _extra = event.value

            # JOYBUTTONUP      joy, button
            elif event_type == pygame.JOYBUTTONUP:
                _action_key = (event.joy, event.button)

            # JOYBUTTONDOWN    joy, button
            elif event_type == pygame.JOYBUTTONDOWN:
                _action_key = (event.joy, event.button)

            # QUIT             none
            elif event_type == pygame.QUIT:
                pass

            # ACTIVEEVENT      gain, state
            elif event_type == pygame.ACTIVEEVENT:
                _action_key = (event.state, event.gain)

            # VIDEORESIZE      size, w, h
            elif event_type == pygame.VIDEORESIZE:
                _extra = (event.w, event.h)

            # VIDEOEXPOSE      none
            elif event_type == pygame.VIDEOEXPOSE:
                pass

            # USEREVENT        code
            elif event_type == pygame.USEREVENT:
                _extra = dict(event.__dict__)
            else:
                _unprocessed_events.append(event)

            _action = _actions.get(_action_key, None)
            if _action is None:
                _entry = (event_type, _action_key)
                _unprocessed_events.append(event)
                if _entry not in self._seen_unmapped:
                    self._seen_unmapped.append(_entry)
                    logger.debug("no mapping found for key: {0} of event {1}", _action_key, event)
            else:
                result.append((_action, _extra))

        return result, _unprocessed_events

    @staticmethod
    def _process_key_event(event_key, event_mod, event_scancode, action_key, action_id, extra_value, result):
        if action_key == event_scancode or action_key == ANY_KEY:
            result.append((action_id, extra_value))
            return True
        elif isinstance(action_key, tuple):
            _mapping_key, _mapping_mod = action_key
            if _mapping_key == event_key and event_mod != NO_MOD and _mapping_mod & event_mod or \
               _mapping_key == event_key and event_mod == NO_MOD and _mapping_mod == NO_MOD or \
               _mapping_key == ANY_KEY and _mapping_mod & event_mod or \
               _mapping_key == ANY_KEY and _mapping_mod == ANY_MOD or \
               _mapping_key == event_key and _mapping_mod == ANY_MOD:
                    result.append((action_id, extra_value))
                    return True
        return False

    def get_mapped_event_types(self):
        """
        Gets the mapped event types.
        :return: [even_type, ]
        """
        return list(self._event_to_action_map.keys())


if __name__ == '__main__':  # pragma: nocover

    ACTION_QUIT = 1
    ACTION_02 = 2
    ACTION_03 = 3
    ACTION_04 = 4
    ACTION_05 = 5
    ACTION_06 = 6
    ACTION_07 = 7
    ACTION_08 = 8
    ACTION_09 = 9
    ACTION_10 = 10
    ACTION_11 = 11
    ACTION_12 = 12
    ACTION_RESIZE = 'resize!'
    ACTION_TOGGLE_EVENTS_SQUELCH = 14

    event_map = {
        pygame.QUIT: {None: ACTION_QUIT},
        pygame.KEYDOWN: {
            (pygame.K_ESCAPE, NO_MOD): ACTION_QUIT,
            (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
            (pygame.K_s, NO_MOD): ACTION_02,
            (pygame.K_a, ANY_MOD): ACTION_12,
            (ANY_KEY, ANY_MOD): ACTION_04,  # map any key down with any modifiers to action 04
            (ANY_KEY, pygame.KMOD_SHIFT): ACTION_03,
        },
        pygame.KEYUP: {
            (pygame.K_d, NO_MOD): 99,
            59: ACTION_TOGGLE_EVENTS_SQUELCH,  # scancode 59 <=> F1
        },
        pygame.MOUSEMOTION: {
            (1, 0, 0): ACTION_05,
            None: ACTION_10
        },
        pygame.MOUSEBUTTONDOWN: {1: ACTION_06, 3: ACTION_07},
        pygame.MOUSEBUTTONUP: {1: ACTION_08, 3: ACTION_09},
        pygame.ACTIVEEVENT: {
            (1, 0): ACTION_10,  # mouse left win
            (1, 1): ACTION_11  # mouse entered win
        },
        pygame.VIDEORESIZE: {None: ACTION_RESIZE}
    }

    import pygame

    pygame.init()
    screen = pygame.display.set_mode((800, 600), pygame.RESIZABLE)

    font = pygame.font.Font(None, 22)
    sprites = []
    text_to_render = []

    mapper = EventMapper(event_map)

    spam_events = True
    running = True
    while running:
        text_to_render.clear()
        sprites = sprites[:500]  # keep only the N most recent events

        events = [pygame.event.wait()]  # don't hog the CPU
        if events and spam_events:
            text_to_render.append("events: {0}".format([_ev for _ev in events]))
            print(text_to_render[-1])
        actions, unhandled_events = mapper.get_actions(events)
        for action, extra in actions:
            text_to_render.append("    =>>> action: {0} {1}".format(action, extra))
            print(text_to_render[-1])

            # react to actions
            if action == ACTION_QUIT:
                running = False
            elif action == ACTION_RESIZE:
                w, h = extra
                screen = pygame.display.set_mode((w, h), pygame.RESIZABLE)
            elif action == ACTION_TOGGLE_EVENTS_SQUELCH:
                spam_events = not spam_events

        for text in text_to_render:
            img = font.render(text, 1, (255, 255, 255))
            spr = pygame.sprite.Sprite()
            spr.image = img
            spr.rect = img.get_rect()
            sprites.insert(0, spr)

        screen.fill((0, 0, 0))  # clear the screen
        _y = screen.get_size()[1]
        for spr in sprites:
            _y -= spr.rect.height + 5
            screen.blit(spr.image, (0, _y))

        pygame.display.flip()

logger.debug("imported")
