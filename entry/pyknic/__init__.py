# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'compatibility.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The pyknic package contains modules, classes, algorithms and infrastructure code to help write games.


The package is subdivided in two parts:

    - pyknic
    - pyknic.pyknic_pygame


.. todo:: explain how logging works and is configured


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version


"""
import sys
import logging

# log info first before importing anything
from pyknic import info
info.log_info()

from pyknic import logs
from pyknic import context
from pyknic import cache
from pyknic import events
from pyknic import generators
from pyknic import mathematics
from pyknic import options
from pyknic import resource
from pyknic import timing
from pyknic import tweening
from pyknic import compatibility
from pyknic import animation
from pyknic import registry

from pyknic.info import log_info, check_version

__all__ = [
    # modules
    "animation",
    "logs",
    "info",
    "context",
    "cache",
    "events",
    "generators",
    "mathematics",
    "options",
    "resource",
    "timing",
    "tweening",
    # functions
    "log_info",
    "check_version",
    "compatibility",
    "registry",
]

logger = logging.getLogger(__name__)

# check python version
# BE WARNED: if you change this line it might work or not (the code is probably untested with new versions)
info.check_version("python", (2, 7), tuple(sys.version_info[:2]), (3, 8), [(3, 0), (3, 1), (3, 2), (3, 3), (3, 4)])

# TODO: write a predefined main loop -> subsystem management? (because of e.g.  music fadeout)
# TODO: logging to another process...?

logger.debug("imported " + "-" * 50)
